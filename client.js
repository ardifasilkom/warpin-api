// socket-client connect to the server
var socket = require('socket.io-client')('http://localhost:8000');
const repl = require('repl');

socket.on('disconnect', function(){
    socket.emit('disconnect')
});

socket.on('connect', () => {
    console.log('=== start chatting ===')
})

socket.on('message', (data) => {
    console.log(data);
})

// using repl for better persistance apps
repl.start({
    prompt: '',
    eval: (cmd) => {
        socket.send(cmd)
    }
})