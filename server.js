const http = require('http').createServer();
const io = require('socket.io')(http);
const port = 8000

// this code bellow initiate long live connectiong
io.on('connection', (socket) => {
    console.log('connected')
    // Server will broadcast messages to all connected client
    socket.on('message', (evt) => {
        console.log(evt)
        socket.broadcast.emit('message', evt)
    })
})

io.on('disconnect', (evt) => {
    console.log('disconnected')
})

http.listen(port, () => console.log('server is listening on port : ' + port))